﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseInteraction : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public Rigidbody rgbody;
    public Renderer rend;

    private Color oldColor;

    public void OnPointerEnter(PointerEventData eventData)
    {
        oldColor = rend.material.color;
        rend.material.color = Color.red;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        rend.material.color = oldColor;
    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        rgbody.AddForce(Camera.main.transform.forward*1000);
    }

    // Start is called before the first frame update
    void Start()
    {
        rgbody = GetComponent<Rigidbody>();
        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
